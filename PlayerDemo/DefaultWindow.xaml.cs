﻿using MahApps.Metro.Controls;
using PlayerDemo.ViewModels;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace PlayerDemo;

/// <summary>
/// DefaultWindow.xaml  MVVM   a simple player style
/// </summary>
public partial class DefaultWindow : MetroWindow
{
    public DefaultViewModel mainViewModel
    {
        get; set;
    }
    private int TimeCount { get; set; }
    public DefaultWindow()
    {
        InitializeComponent();
        mainViewModel = new DefaultViewModel(this);
        this.DataContext = mainViewModel;
        host.Overlay.SizeChanged += Overlay_SizeChanged;
        host.Overlay.WindowStyle = WindowStyle.None;
        if (host.Surface != null)
        {
            host.Surface.MouseMove += Surface_MouseMove;
        }
    }

    bool IsOpenStoryboard = false;
    bool IsCloseStoryboard = false;
    private void Surface_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
    {
        Mouse.OverrideCursor = Cursors.Arrow;
        if (hostGrid.Opacity == 0)
        {
            if (!IsOpenStoryboard)
            {
                IsOpenStoryboard = true;
                var opacityDoubleAnimation = new DoubleAnimation
                {
                    Duration = new TimeSpan(0, 0, 0, 0, 500),
                    DecelerationRatio = 0.7,
                    From = 0.0,
                    To = 1.0,
                    FillBehavior = FillBehavior.Stop
                };
                opacityDoubleAnimation.Completed += (o, e) =>
                {
                    IsOpenStoryboard = false;
                };
                hostGrid.BeginAnimation(OpacityProperty, opacityDoubleAnimation);


                TimeCount = 0;
            }
        }
    }

    private void Overlay_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
    {
        if (sender is Window Overlay)
        {
            mainViewModel.PlayerControlWidth = Overlay.ActualWidth * 0.8;
        }

    }

    private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
        TimeCount++;
        if (TimeCount > 3 && hostGrid.Opacity == 1)
        {
            if (!IsCloseStoryboard)
            {
                IsCloseStoryboard = true;
                var opacityDoubleAnimation = new DoubleAnimation
                {
                    Duration = new TimeSpan(0, 0, 0, 0, 1000),
                    DecelerationRatio = 0.7,
                    From = 1.0,
                    To = 0.0,
                };
                opacityDoubleAnimation.Completed += (o, e) =>
                {
                    IsCloseStoryboard = false;
                    Mouse.OverrideCursor = Cursors.None;
                };
                hostGrid.BeginAnimation(OpacityProperty, opacityDoubleAnimation);


            }

        }
    }
}
